using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public int hp = 10;

    Animator anim;
    Collider col;
    NavMeshAgent agent;
    Navigator nav;

    Collider[] colliders;
    Rigidbody[] rigidbodies;

    private void Start()
    {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider>();
        agent = GetComponent<NavMeshAgent>();
        nav = GetComponent<Navigator>();

        colliders = GetComponentsInChildren<Collider>();
        rigidbodies = GetComponentsInChildren<Rigidbody>();

        foreach (Collider collider in colliders)
        {
            collider.enabled = false;
        }
        foreach(Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = true;
        }
        col.enabled = true;
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;

        if(hp <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        anim.enabled = false;
        agent.enabled = false;
        nav.enabled = false;

        foreach (Collider collider in colliders)
        {
            collider.enabled = true;
        }
        foreach (Rigidbody rb in rigidbodies)
        {
            rb.isKinematic = false;
        }
        col.enabled = false;
    }
}
