﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float speed = 5;
    public float jumpForce = 10;
    public Weapon currentWeapon;

    Rigidbody rb;
    Camera cam;

    float rotY;
    float rotX;

    bool isGrounded = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;

        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        
        float mousex = Input.GetAxis("Mouse X");
        float mousey = Input.GetAxis("Mouse Y");

        isGrounded = Physics.Raycast(transform.position, Vector3.down, 1.2f);
        Debug.DrawRay(transform.position, Vector3.down * 1.2f, Color.red);

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        Debug.DrawRay(cam.transform.position, cam.transform.forward * 999, Color.red);

        if (Input.GetButtonDown("Fire1"))
        {
            RaycastHit hit;
            Physics.Raycast(cam.transform.position, cam.transform.forward, out hit);
            
            currentWeapon.Fire(hit);

        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            currentWeapon.Reload();
        }

        rotY += mousex;
        rotX += mousey;

        rotX = Mathf.Clamp(rotX, -90, 90);

        rb.velocity = new Vector3(0, rb.velocity.y, 0) + (transform.forward * v + transform.right * h) * speed;
        

        transform.rotation = Quaternion.Euler(0, rotY, 0);
        cam.transform.localRotation = Quaternion.Euler(-rotX, 0, 0);
    }
}
