using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int damage = 5;
    public int currentAmmo = 10;
    public int maxAmmo = 20;
    public float shootForce = 10;
    public LineRenderer shootLine;
    public Transform shootPoint;
    public GameObject muzzleFlash;

    public void Fire(RaycastHit hit)
    {
        if (currentAmmo <= 0) return;
        currentAmmo--;
        shootLine.enabled = true;
        muzzleFlash.SetActive(true);

        shootLine.SetPosition(0, shootPoint.position);

        if(hit.transform != null)
        {

            Rigidbody rb = hit.transform.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(shootForce, hit.point, 0.2f, 0.2f, ForceMode.VelocityChange);
            }

            Enemy enemy = hit.transform.GetComponent<Enemy>();
            if(enemy)
            {
                enemy.TakeDamage(damage);
            }

            shootLine.SetPosition(1, hit.point);
        }
        else
        {
            shootLine.SetPosition(1, Camera.main.transform.forward * 9999);
        }

        StartCoroutine(DisableLine());
    }

    public void Reload()
    {
        currentAmmo = maxAmmo;
    }

    IEnumerator DisableLine()
    {
        yield return new WaitForSeconds(0.05f);
        shootLine.enabled = false;
        muzzleFlash.SetActive(false);
    }
}
